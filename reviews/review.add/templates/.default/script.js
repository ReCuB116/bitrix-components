$('html').on('submit', '.addComment', function (e) {
  console.log('success');
  e.preventDefault();
  $(this).find('input[name="JS"]').remove();
  $.ajax({
    url: $(this).attr('action'),
    type: 'post',
    data: $(this).serializeArray(),
    dataType: 'json',
    cache: false,
    success: function (data) {
      if (data.result = 'true') {
        alert('Ваш отзыв в процессе модерации');
        location.reload();
      }
      else {
        alert('Ошибка со стороны сервера');
      }
    },
    error: function () {
      alert('Ошибка');
    }
  });
});