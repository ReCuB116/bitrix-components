<?php
/**
 * Created by PhpStorm.
 * User: CuBeR116
 * Date: 08.12.2018
 * Time: 14:06
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if ($USER->GetID()):
  ?>
  <div class="container">
    <h2 class="mt-4">Добавить отзыв</h2>
    <form action="/local/components/reviews/addComment.php"
          class="addComment"
          method="post">
      <div class="ratingBody">
        <input type="radio"
               name="rating"
               id="rating-5"
               value="5">
        <label class="star rating-1"
               for="rating-5"></label>
        <input type="radio"
               name="rating"
               id="rating-4"
               value="4">
        <label class="star rating-4"
               for="rating-4"></label>
        <input type="radio"
               name="rating"
               id="rating-3"
               value="3">
        <label class="star rating-3"
               for="rating-3"></label>
        <input type="radio"
               name="rating"
               id="rating-2"
               value="2">
        <label class="star rating-2"
               for="rating-2"></label>
        <input type="radio"
               name="rating"
               id="rating-1"
               value="1">
        <label class="star rating-1"
               for="rating-1"></label>
      </div>

      <div class="moreInfo">
        <? if ($arResult['IBLOCK_ID'] || $arResult['IBLOCK_ID'] != 'N'): ?>

          <input type="hidden"
                 name="iblock_id"
                 value="<?= $arResult['IBLOCK_ID'] ?>">

        <? endif; ?>

        <input type="hidden"
               name="element_id"
               value="<?= $arResult['ID'] ?>">

        <input type="hidden"
               name="JS"
               value="false">
      </div>

      <div class="row">
        <div class="col-12">
          <label for="reviewText">Ваш отзыв</label>
          <textarea name="reviewText"
                    id="reviewText"
                    required></textarea>
        </div>
      </div>
      <input type="submit"
             value="Отправить">
    </form>
  </div>
<? endif; ?>

