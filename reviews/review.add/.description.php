<?php
/**
 * Created by PhpStorm.
 * User: CuBeR116
 * Date: 08.12.2018
 * Time: 14:02
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$arComponentDescription = [
  "NAME"        => "Добавить отзыв",
  "DESCRIPTION" => "Добавление отзыва для элемента",
  "PATH"        => [
    "ID"   => "Отзывы",
    "CHILD" => [
      "ID" => "add",
      "NAME" => "Добавить отзыв"
    ]
  ],
  "ICON"        => "/images/icon.gif",
];
?>