<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if (!CModule::IncludeModule("iblock"))
  return;

$arTypesEx = CIBlockParameters::GetIBlockTypes(["-" => " "]);


$arIBlocks = [];
$db_iblock = CIBlock::GetList(["SORT" => "ASC"], ["SITE_ID" => $_REQUEST["site"], "TYPE" => ($arCurrentValues["IBLOCK_TYPE"] != "-" ? $arCurrentValues["IBLOCK_TYPE"] : "")]);
while ($arRes = $db_iblock->Fetch())
  $arIBlocks[$arRes["ID"]] = "[" . $arRes["ID"] . "] " . $arRes["NAME"];


$arSorts = ["ASC" => GetMessage("T_IBLOCK_DESC_ASC"), "DESC" => GetMessage("T_IBLOCK_DESC_DESC")];
$arSortFields = [
  "ID"          => GetMessage("T_IBLOCK_DESC_FID"),
  "NAME"        => GetMessage("T_IBLOCK_DESC_FNAME"),
  "ACTIVE_FROM" => GetMessage("T_IBLOCK_DESC_FACT"),
  "SORT"        => GetMessage("T_IBLOCK_DESC_FSORT"),
  "TIMESTAMP_X" => GetMessage("T_IBLOCK_DESC_FTSAMP")
];

$arProperty_LNS = [];
$rsProp = CIBlockProperty::GetList(["sort" => "asc", "name" => "asc"], ["ACTIVE" => "Y", "IBLOCK_ID" => (isset($arCurrentValues["IBLOCK_ID"]) ? $arCurrentValues["IBLOCK_ID"] : $arCurrentValues["ID"])]);
while ($arr = $rsProp->Fetch()) {
  $arProperty[$arr["CODE"]] = "[" . $arr["CODE"] . "] " . $arr["NAME"];
  if (in_array($arr["PROPERTY_TYPE"], ["L", "N", "S"])) {
    $arProperty_LNS[$arr["CODE"]] = "[" . $arr["CODE"] . "] " . $arr["NAME"];
  }
}

$arComponentParameters = [
  "GROUPS"     => [],
  "PARAMETERS" => [
    "IBLOCK_TYPE" => [
      "PARENT"  => "BASE",
      "NAME"    => "Тип информационного блока:",
      "TYPE"    => "LIST",
      "VALUES"  => $arTypesEx,
      "DEFAULT" => "news",
      "REFRESH" => "Y",
    ],
    "R_IBLOCK_ID"   => [
      "PARENT"            => "BASE",
      "NAME"              => "Код информационного блока",
      "TYPE"              => "LIST",
      "VALUES"            => $arIBlocks,
      "DEFAULT"           => '={$_REQUEST["ID"]}',
      "ADDITIONAL_VALUES" => "Y",
      "REFRESH"           => "Y",
    ],
    "ID"          => [
      "PARENT" => "BASE",
      "NAME"   => "ID Отзывов",
      "TYPE"   => "STRING"
    ],
  ]
];

?>


