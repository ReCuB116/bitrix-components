<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arResult['ID'] = trim($arParams["ID"]);
$arResult['R_IBLOCK_ID'] = trim($arParams['R_IBLOCK_ID']);
$arResult['SHOW_UNMODERATED'] = trim($arParams['SHOW_UNMODERATED']);

$this->IncludeComponentTemplate();
