<?php
/**
 * Created by PhpStorm.
 * User: CuBeR116
 * Date: 08.12.2018
 * Time: 14:02
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentDescription = array(
  "NAME" => "Список отзывов",
  "DESCRIPTION" => "Показать список отзывов",
  "PATH" => array(
    "ID" => "Отзывы",
    "CHILD" => [
      "ID" => "SHOW",
      "NAME" => "Список отзывов"
    ]
  ),
  "ICON" => "/images/icon.gif",
);
?>