<?php
/**
 * Created by PhpStorm.
 * User: CuBeR116
 * Date: 08.12.2018
 * Time: 14:26
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arSelect = ["ID", "NAME", "IBLOCK_ID", "DATE_CREATE", "DETAIL_TEXT", "PROPERTY_*"];

$arFilter = [
  "IBLOCK_ID" => $arResult['R_IBLOCK_ID'],
  "ACTIVE_DATE" => "Y",
  "ACTIVE" => $arResult['SHOW_UNMODERATED'],
  "PROPERTY_R_ELEMENT_ID" => $arResult['ID']];

$res = CIBlockElement::GetList([], $arFilter, false, [], $arSelect);
while ($ob = $res->GetNextElement()) {
  $arResult["REVIEWS"][] = [
    "BASE"     => $ob->GetFields(),
    "PROPERTY" => $ob->GetProperties(),
  ];

}