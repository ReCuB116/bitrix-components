<?php
/**
 * Created by PhpStorm.
 * User: CuBeR116
 * Date: 08.12.2018
 * Time: 14:06
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

?>
<? if ($arResult['REVIEWS']): ?>
  <div class="container">
    <h2 class="mt-4">Отзывы</h2>
    <? foreach ($arResult['REVIEWS'] as $key => $ELEMENTS) : ?>

      <?
      $rsUser = CUser::GetByID($ELEMENTS['PROPERTY']['R_NAME']['VALUE']);
      $arUser = $rsUser->Fetch();
      ?>

      <p>ID Пользователя - <strong><?= $ELEMENTS['PROPERTY']['R_NAME']['VALUE'] ?></strong></p>
      <p>Логин пользователя - <strong><?= $arUser['LOGIN'] ?></strong></p>
      <p>Дата отзыва - <strong></strong><?= $ELEMENTS['BASE']['DATE_CREATE'] ?></strong></p>
      <? if ($ELEMENTS['PROPERTY']['R_ANSWER']['VALUE']): ?>
        <blockquote>
      <? endif; ?>

      <p>Текст отзыва - <strong><?= $ELEMENTS['BASE']['DETAIL_TEXT'] ?></strong></p>

      <? if ($ELEMENTS['PROPERTY']['R_ANSWER']['VALUE']): ?>
        </blockquote>
        <p><?= $ELEMENTS['PROPERTY']['R_ANSWER']['VALUE'] ?></p>
      <? endif; ?>

      <hr>
    <? endforeach; ?>
  </div>
<? endif; ?>