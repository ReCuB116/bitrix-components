<?php
/**
 * Created by PhpStorm.
 * User: CuBeR116
 * Date: 08.12.2018
 * Time: 14:02
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$arComponentDescription = [
  "NAME"        => "Средняя оценка",
  "DESCRIPTION" => "Средняя оценка для элемента",
  "PATH"        => [
    "ID"   => "Отзывы",
    "CHILD" => [
      "ID" => "average",
      "NAME" => "Средняя оценка",
    ]
  ],
  "ICON"        => "/images/icon.gif",
];
?>