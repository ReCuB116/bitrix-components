<?php
/**
 * Created by PhpStorm.
 * User: CuBeR116
 * Date: 08.12.2018
 * Time: 14:06
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>

<? if ($arResult['REVIEWS']['AVEGARE']): ?>
  <div class="container">
    <p>Средняя оценка элемента - <strong><?= $arResult['REVIEWS']['AVEGARE'] ?></strong></p>
  </div>
<? endif; ?>