<?php
/**
 * Created by PhpStorm.
 * User: CuBeR116
 * Date: 08.12.2018
 * Time: 14:26
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

CModule::IncludeModule("iblock");

$arSelect = ["ID", "NAME", "IBLOCK_ID", "DATE_CREATE", "PROPERTY_R_RATING"];

$arFilter = [
  "ACTIVE_DATE"           => "Y",
  "ACTIVE"                => "Y",
  "PROPERTY_R_ELEMENT_ID" => $arResult['ID']
];

$res = CIBlockElement::GetList([], $arFilter, false, [], $arSelect);
while ($ob = $res->GetNextElement()) {
  $arResult["REVIEWS"][] = [
    "BASE"     => $ob->GetFields(),
    "PROPERTY" => $ob->GetProperties()
  ];

}
$sum = floatval(0);
$i = count($arResult['REVIEWS']);

if ($arResult['REVIEWS']) {
  foreach ($arResult['REVIEWS'] as $key => $ELEMENTS) {
    $sum += $ELEMENTS['PROPERTY']['R_RATING']['VALUE'];
  }

  $sum = round($sum / $i, 1);
  $arResult['REVIEWS']['AVEGARE'] = $sum;

}

//templFUNC::debug($arResult["REVIEWS"]);