<?php
/**
 * Created by PhpStorm.
 * User: CuBeR116
 * Date: 05.12.2018
 * Time: 19:24
 */

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
  CModule::IncludeModule("iblock");
  $el = new CIBlockElement;

  array_walk($_POST, 'clean');

  $JS = $_POST['JS'];

  $PROP = [];
  $PROP['R_ELEMENT_ID'] = $_POST['element_id'];     // свойству с кодом 44 - ID элемента
  $PROP['R_NAME'] = $USER->GetID();  // свойству с кодом 45 - Имя пользователя

  if ($_POST['rating'])
    $PROP['R_RATING'] = $_POST['rating'];        // свойству с кодом 46 - Рейтинг


  $arLoadProductArray = [
    "MODIFIED_BY"       => $USER->GetID(), // элемент изменен текущим пользователем
    "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
    "IBLOCK_ID"         => 20,
    "PROPERTY_VALUES"   => $PROP,
    "NAME"              => 'Отзыв - ' . $USER->GetLogin(),
    "DETAIL_TEXT"       => $_POST['reviewText'],
    "ACTIVE"            => "N", // Не активен
  ];

  if ($PRODUCT_ID = $el->Add($arLoadProductArray)) {
    
    if (empty($JS)) {
      echo json_encode([
        'result' => 'true'
      ]);
    } elseif ($JS) {
      header("refresh: 2; url=" . $_SERVER['HTTP_REFERER']);
      echo 'Ваш отзыв добавлен, вы будете перенаправлены';
    }

  } else {
    echo json_encode([
      'result' => 'false'
    ]);
  }

  function clean($string) {
    return strip_tags($string);
  }
}

?>